# rusty-chat

rusty-chat is a small TUI (text UI) chat server and client written in Rust as an exercise. The pancurses crate is used to abstract Windows/*NIX differences.

## Compilation

This program uses ncurses, and should work on Linux, Windows, and Mac.

```bash
$ git clone https://gitlab.com/avkhanov/rusty-chat.git
$ cd rusty-chat
$ cargo build
```

## Running

First, you need to start a server. Currently, the server is hardcoded to listen on port 12345.

```bash
$ cargo run --bin rusty-chat-server
```

Then, you can run clients and point them to the server. **To quit the program, press the ESC key**.
On Linux/Mac, the client will run in the terminal. On Windows however, a new window will be launched (since pdcurses is used to provide ncurses on Windows)

```bash
$ cargo run --bin rusty-chat <USERNAME> <HOST:PORT>
```

For example:

```bash
$ cargo run --bin rusty-chat QuickBen localhost:12345
```
```bash
$ cargo run --bin rusty-chat Whiskeyjack 10.243.53.130:12345
```
```bash
$ cargo run --bin rusty-chat Fiddler my.fun.server:12345
```



