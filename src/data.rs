use serde_json;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum RCMessage {
    // Client Messages
    //Join { username: String, client_port: String },
    Join(String), // Send Username
    Leave(String), // Send UUID
    SendMessage { uuid: String, message: String },
    SendPrivateMessage { uuid: String, username: String, message: String},
    Command { uuid: String, command: String, args: Vec<String> },
    RequestUserList,

    // Server Messages
    Ok,
    JoinAccepted(String), // Returns UUID
    Error(String),
    UserJoined(String),
    UserLeft(String),
    Message { from_username: String, message: String },
    ServerNotification(String),
    PrivateMessage { username: String, message: String },
    ServerDown(String),
    UserList(Vec<String>)
}

pub fn serialize(msg: RCMessage) -> Vec<u8> {
    (serde_json::to_string(&msg).unwrap() + "\n").into_bytes()
}

pub fn deserialize(msg: String) -> Result<RCMessage, String> {
    match serde_json::from_str(msg.as_str()) {
        Ok(m) => Ok(m),
        Err(_) => Err(format!("Message format of '{}' is incorrect", msg).to_string())
    }
}

