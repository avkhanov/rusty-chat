#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate uuid;

mod data;

use std::net::{TcpListener, TcpStream};
use data::{RCMessage, serialize, deserialize};
use std::sync::{Arc, Mutex};
use std::thread;
use std::cell::RefCell;
use std::collections::HashMap; 
use std::collections::hash_map::Values;
use uuid::Uuid;

use std::io::{Read, Write};

// User
//
struct User {
    uuid: String,
    username: String,
    __socket: Arc<Mutex<TcpStream>>,
    __incoming: Arc<Mutex<Vec<RCMessage>>>,
    __outgoing: Arc<Mutex<Vec<RCMessage>>>,
    __unwind: Arc<Mutex<bool>>,
    __thread: Option<thread::JoinHandle<()>>
}

impl User {
    fn new(username: String, uuid: String, socket: TcpStream) -> User {
        let mut user = User { 
            uuid: uuid,
            username: username, 
            __socket: Arc::new(Mutex::new(socket)),
            __incoming: Arc::new(Mutex::new(Vec::new())),
            __outgoing: Arc::new(Mutex::new(Vec::new())),
            __unwind: Arc::new(Mutex::new(false)),
            __thread: None
        };
        
        let t_socket = user.__socket.clone();
        let t_incoming = user.__incoming.clone();
        let t_outgoing = user.__outgoing.clone();
        let t_unwind = user.__unwind.clone();

        let h = thread::spawn(move || {
            // Enable non-blocking reads
            t_socket.lock().unwrap().set_nonblocking(true).expect("Could not enable non-blocking mode on socket");

            // Read buffer
            let mut received_bytes = Vec::<u8>::new();

            loop {
                // Check if we need to write something
                let mut out = t_outgoing.lock().unwrap();
                for msg in out.drain(..) {
                    match t_socket.lock().unwrap().write(&serialize(msg)) {
                        Err(e) => println!("{}", e),
                        _ => {}
                    };
                };

                // Attempt to read
                let mut buf = [0u8];
                match t_socket.lock().unwrap().read(&mut buf) {
                    Ok(0) => {},
                    Ok(_) => match buf[0] as char {
                        '\n' => { // We have a message!
                            match String::from_utf8(received_bytes.clone()) {
                                Ok(s) => match deserialize(s) {
                                    Ok(msg) => t_incoming.lock().unwrap().push(msg),
                                    Err(e) => println!("{}", e)
                                },
                                Err(_) => println!("Corrupt message from user")
                            };
                            received_bytes.clear();
                        },
                        '\r' => {}, // Do nothing
                        c @ _ => received_bytes.push(c as u8)
                    },
                    _ => {}
                }
                
                
                // Check if we're done
                if *t_unwind.lock().unwrap() == true {
                    break;
                }
            }
        });

        user.__thread = Some(h);

        user
    }

    fn write(&self, msg: RCMessage) {
        self.__outgoing.lock().unwrap().push(msg);
    }

    fn read(&self) -> Option<RCMessage> {
        self.__incoming.lock().unwrap().pop()
    }
}

impl Drop for User {
    fn drop(&mut self) {
        println!("Dropping user record for '{}'...", self.username);
        *self.__unwind.lock().unwrap() = true;
        match self.__thread.take() {
            Some(h) => { 
                match h.join() {
                    Err(_) => println!("Could not close user thread!"),
                    _ => {}
                }
            },
            None => {}
        };
        println!("...Done");
    }
}

struct Users {
    __uuid_to_user: HashMap<String, User>,
    __username_to_uuid: HashMap<String, String>,
    __to_drop: RefCell<Vec<String>>
}

impl Users {
    fn new() -> Users {
        Users { 
            __uuid_to_user: HashMap::new(), 
            __username_to_uuid: HashMap::new(),
            __to_drop: RefCell::new(Vec::new())
        }
    }

    fn add(&mut self, uuid: String, user: User) -> Result<(), String> {
        match self.exists(&user.username) {
            false => { 
                user.write(RCMessage::JoinAccepted(uuid.clone())); 
                self.__username_to_uuid.insert(user.username.clone(), uuid.clone());
                self.__uuid_to_user.insert(uuid, user);
                Ok(()) 
            },
            true => {
                let err_msg = format!("User '{}' already exists", user.username);
                //user.socket.write(&serialize(RCMessage::Error(err_msg.to_string())));
                user.write(RCMessage::Error(err_msg.to_string()));
                Err(err_msg.to_string())
            }
        }
    }
    
    fn get_user(&self, user: &String) -> Option<&User> {
        if self.exists(user) {
            return Some(&self.__uuid_to_user[&self.__username_to_uuid[user]])
        } else {
            return None
        }
    }

    fn exists(&self, user: &String) -> bool {
        self.__username_to_uuid.contains_key(user)
    }

    fn uuid_exists(&self, uuid: &String) -> bool {
        self.__uuid_to_user.contains_key(uuid)
    }

    //fn iter(&mut self) -> ValuesMut<String, User> {
    fn iter(&self) -> Values<String, User> {
        //return self.__uuid_to_user.values_mut();
        return self.__uuid_to_user.values();
    }

    fn drop_marked(&mut self) {
        let mut iterator = self.__to_drop.borrow_mut();

        for i in iterator.drain(..) {
            {
                let username = &self.__uuid_to_user[&i].username;
                self.__username_to_uuid.remove(username);

                for i in self.__uuid_to_user.values() {
                    i.write(RCMessage::UserLeft(username.clone()));
                }
            }
            self.__uuid_to_user.remove(&i);
        }
    }

    fn mark_drop(&self, uuid: String) {
        self.__to_drop.borrow_mut().push(uuid);
    }

    fn get_user_list(&self) -> Vec<String> {
        let mut ret = Vec::<String>::new();

        for i in self.__username_to_uuid.keys() {
            ret.push(i.clone());
        }

        ret.sort();
        ret
    }
}


// Server Logic
//
fn main() {
    let listener = TcpListener::bind("0.0.0.0:12345").unwrap();
    let users = Arc::new(Mutex::new(Users::new()));

    let t_users = users.clone();

    thread::spawn(move || {
        loop {
            let ref mut users = *t_users.lock().unwrap();

            for user in users.iter() {
                match user.read() {
                    Some(msg) => handle_message(msg, &user, users),
                    _ => {}
                }
            }

            users.drop_marked();

        }
    });

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                match new_user(stream, users.clone()) {
                    Err(e) => println!("{}", e),
                    _ => {}
                }
            },
            Err(_) => {}
        }
    }
}

fn new_user(stream: TcpStream, users: Arc<Mutex<Users>>) -> Result<(),String> {
    if let Ok(s) = read_line_blocking(&stream) {
        return match deserialize(s) {
            //Ok(RCMessage::Join { username, client_port }) => { 
            Ok(RCMessage::Join(username)) => {
                let uuid = Uuid::new_v4().hyphenated().to_string();
                let mut _users = users.lock().unwrap();

                match _users.add(uuid.clone(), User::new(username.clone(), uuid, stream)) {
                    Ok(_) => {
                        for i in _users.iter() {
                            i.write(RCMessage::UserJoined(username.clone()));
                        }
                        Ok(())
                    },
                    _ => Err("Could not add user".to_string())
                }
            },
            _ => Err("Expected 'Join' message".to_string())
        };
    } else {
        return Err("Did not receive any message".to_string());
    }

    //Ok(())
}

fn read_line_blocking(stream: &TcpStream) -> Result<String, String> {
    let mut bytes = Vec::<u8>::new();

    for i in stream.bytes() {
        match i {
            Ok(v) if v == '\n' as u8  => break,
            Ok(v) if v == '\r' as u8  => (),
            Ok(v) => bytes.push(v),
            Err(_) => return Err("Ran out of bytes waiting for newline".to_string())
        }
    }

    if bytes.is_empty() {
        return Err("Nothing left to read".to_string());
    }

    match String::from_utf8(bytes) {
        Ok(v) => Ok(v),
        Err(_) => Err("Could not convert bytes to UTF-8 string".to_string())
    }
}

fn handle_message(msg: RCMessage, user: &User, users: &Users) {
    match msg {
        RCMessage::SendMessage { uuid, message } => {
            if users.uuid_exists(&uuid) && user.uuid == uuid {
                for i in users.iter() {
                    i.write(RCMessage::Message { from_username: user.username.clone(), message: message.clone() });
                }
            } else {
                user.write(RCMessage::Error("Message rejected".to_string()));
            }
        },
        RCMessage::Leave(uuid) => {
            users.mark_drop(uuid);

        },
        RCMessage::RequestUserList => {
            user.write(RCMessage::UserList(users.get_user_list()));
        },
        RCMessage::Command { uuid, command, args } => {
            if users.uuid_exists(&uuid) && user.uuid == uuid {
                match command.as_str() {
                    "whisper" => whisper(user, users, args),
                    _ => {}
                }
            } else {
                user.write(RCMessage::Error("Message rejected".to_string()));
            }
        },
        m @ _ => println!("Unhandled message - {:?}", m)
    };
}

fn whisper(user: &User, users: &Users, mut args: Vec<String>) {
    if args.len() < 1 {
        user.write(RCMessage::Error("Username of recepient required".to_string()));
    } else if args.len() < 2 {
        user.write(RCMessage::Error("Message required".to_string()));
    } else {
        let recepient = args.remove(0);
        if let Some(channel) = users.get_user(&recepient) {
            channel.write(RCMessage::PrivateMessage { username: user.username.clone(), message: args.join(" ") });
        }
    }
}

