#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate pancurses;

extern crate uuid;

mod data;

use pancurses::{initscr, endwin, newwin, start_color, use_default_colors, raw};
use pancurses::Window as pnWindow;
use pancurses::Input::{Character, KeyEnter};

use data::{RCMessage, serialize, deserialize};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration, SystemTime};
use std::net::{TcpStream};
use std::io::{Write, Read};


struct Window {
    __window: pnWindow
}

impl Window {
    fn new(height: i32, width: i32, y: i32, x: i32) -> Window {
        let window = newwin(height, width, y, x);
        // TODO: Remove 
        // wmove(window, 1, 1);
        window.mv(1, 1);

        let mut ret = Window { __window: window };
        ret.update();
        ret
    }

    fn raw(&self) -> &pnWindow {
        &self.__window
    }

    fn update(&mut self) {
        // TODO: Remove 
        // box_(self.__window, 0, 0);
        //self.__window.draw_box(0, 0);
        self.__window.border(0, 0, 0, 0, 0, 0, 0, 0);
        //refresh();
        // TODO: Remove
        // wrefresh(self.__window);
        self.__window.refresh();
    }

    fn clear(&mut self) {
        // TODO: Remove
        self.__window.clear();
        self.update();
    }

    fn println(&mut self, string: &str) {
        // TODO: Remove
        // wprintw(self.__window, string.as_ref());
        // wprintw(self.__window, "\n".as_ref());
        self.__window.printw(&string);
        self.__window.printw("\n");

        // TODO: Remove
        // getyx(self.__window, &mut y, &mut x);
        // wmove(self.__window, y, 1);
        self.__window.mv(self.__window.get_cur_y(), 1);

        self.update();
    }
}

fn create_text_box(parent: &pnWindow, username: &String) -> Window {
    let height = 3i32;
    let uname_size = username.len() as i32;

    // TODO: Remove
    // getmaxyx(stdscr(), &mut max_y, &mut max_x);
    // mvprintw(max_y - 2, 1, format!("{}", username).as_ref());
    // refresh();
    let (max_y, max_x) = parent.get_max_yx();
    parent.mvprintw(max_y - 2, 1, &username);
    parent.refresh();

    let win = Window::new(height, max_x - uname_size - 2, max_y - height, uname_size + 2);
    win
}

fn create_chat_box(parent: &pnWindow) -> Window {
    let (max_y, max_x) = parent.get_max_yx();
    let height = max_y - 3;
    //let win = Window::new(height, max_x, 0, 0);
    let win = Window::new(height, max_x - 20, 0, 0);
    win
}

fn create_user_box(parent: &pnWindow) -> Window {
    let (max_y, max_x) = parent.get_max_yx();
    let height = max_y - 3;
    //let win = Window::new(height, max_x, 0, 0);
    let win = Window::new(height, 20, 0, max_x - 20);
    win
}


struct Communicator {
    __incoming: Arc<Mutex<Vec<RCMessage>>>,
    __outgoing: Arc<Mutex<Vec<RCMessage>>>,
    __unwind: Arc<Mutex<bool>>,
    __thread: Option<thread::JoinHandle<()>>
}

impl Communicator {
    fn new(address: String) -> Communicator {
        let mut comm = Communicator {
            __incoming: Arc::new(Mutex::new(Vec::new())),
            __outgoing: Arc::new(Mutex::new(Vec::new())),
            __unwind: Arc::new(Mutex::new(false)),
            __thread: None
        };

        let t_outgoing = comm.__outgoing.clone();
        let t_incoming = comm.__incoming.clone();
        let t_unwind = comm.__unwind.clone();

        let h = thread::spawn(move || {
            let mut stream = TcpStream::connect(address).unwrap();

            // Enable non-blocking reads
            stream.set_nonblocking(true).expect("Could not enable non-blocking mode on socket");

            // Read buffer
            let mut received_bytes = Vec::<u8>::new();

            loop {
                // Check if we need to write something
                let mut out = t_outgoing.lock().unwrap();
                for msg in out.drain(..) {
                    match stream.write(&serialize(msg)) {
                        Ok(_) => {},
                        Err(_) => { /* TODO: Handle the error somehow? */ }
                    }
                };

                // Attempt to read
                let mut buf = [0u8];
                match stream.read(&mut buf) {
                    Ok(0) => {},
                    Ok(_) => match buf[0] as char {
                        '\n' => { // We have a message!
                            //println!("We got something?");
                            match String::from_utf8(received_bytes.clone()) {
                                Ok(s) => match deserialize(s) {
                                    Ok(msg) => t_incoming.lock().unwrap().push(msg),
                                    Err(_) => {}
                                },
                                Err(_) => {}
                            };
                            received_bytes.clear();
                        },
                        '\r' => {}, // Do nothing
                        c @ _ => received_bytes.push(c as u8)
                    },
                    _ => {}
                }
                
                
                // Check if we're done
                if *t_unwind.lock().unwrap() == true {
                    break;
                }
            }
        });

        comm.__thread = Some(h);

        comm
    }

    fn write(&self, msg: RCMessage) {
        self.__outgoing.lock().unwrap().push(msg);
    }

    fn read(&self) -> Option<RCMessage> {
        self.__incoming.lock().unwrap().pop()
    }

    fn read_blocking(&self, timeout: Duration) -> Option<RCMessage> {
        let now = SystemTime::now();
        let mut result = None;

        while result.is_none() && now.elapsed().unwrap() < timeout {
            result = self.read();
        }

        result
    }
}

impl Drop for Communicator {
    fn drop(&mut self) {
        *self.__unwind.lock().unwrap() = true;
        match self.__thread.take() {
            Some(h) => { h.join().unwrap(); },
            None => {}
        };
    }
}

fn main() {
    let mut args = std::env::args();

    // Get rid of executable name
    args.next();

    let username = match args.next() {
        Some(x) => x,
        None => { println!("Missing username"); return }
    };

    let server_host = match args.next() {
        Some(x) => x,
        None => { println!("Missing server address"); return } 
    };

    let root_window = initscr();
    start_color();
    use_default_colors();
    raw();

    let mut text_box = create_text_box(&root_window, &username);
    let mut chat_box = create_chat_box(&root_window);
    let mut user_box = create_user_box(&root_window);

    text_box.raw().nodelay(true);
    root_window.refresh();

    let mut cur_pos = 1;
    let mut str_to_send = String::new();
    
    let comm = Communicator::new(server_host);
    let uuid;

    comm.write(RCMessage::Join(username));

    match comm.read_blocking(Duration::new(5, 0)) {
        Some(msg) => match msg {
            RCMessage::JoinAccepted(s) => { uuid = s; },
            RCMessage::Error(s) => { gui_critical_error(&mut chat_box, &s); return; }
            _ => { gui_critical_error(&mut chat_box, "Bad response from server"); return; }
        },
        None => { gui_critical_error(&mut chat_box, "No response from server"); return; }
    }

    comm.write(RCMessage::RequestUserList);

    loop {
        text_box.raw().mv(1, cur_pos);
        if let Some(c) = text_box.raw().getch() {
            match c {
                Character(ch) if ch == (27 as char) => break,
                KeyEnter | Character('\r') | Character('\n') => {
                    match str_to_send.chars().next() {
                        Some('/') => {
                            let mut words = str_to_send.split_whitespace();
                            let mut command = words.next().unwrap().to_string();
                            command.remove(0);
                            let mut args = Vec::<String>::new();

                            for i in words {
                                args.push(i.to_string());
                            }

                            comm.write(RCMessage::Command { uuid: uuid.clone(), command: command, args: args });
                        },
                        Some(_) => comm.write(RCMessage::SendMessage { uuid: uuid.clone(), message: str_to_send.clone() }),
                        None => {}
                    };
                    str_to_send.clear();
                    text_box.clear();
                    cur_pos = 1; },
                Character(ch) =>  {
                    //str_to_send.push((ch as u8) as char);
                    str_to_send.push(ch);
                    cur_pos += 1;},
                _ => {} 
            }
        }

        match comm.read() {
            Some(msg) => process_message(&mut chat_box, &mut user_box, &comm, msg),
            None => {}
        };
    }
    
    comm.write(RCMessage::Leave(uuid));
    endwin();
}

fn gui_critical_error(window: &mut Window, error: &str) {
    window.println(error);
    window.raw().nodelay(false);
    window.raw().getch();

    endwin();
}

fn process_message(window: &mut Window, user_win: &mut Window, comm: &Communicator, msg: RCMessage) {
    match msg {
        RCMessage::Message { from_username, message } => {
            window.println(&format!("{}: {}", from_username, message));
        },
        RCMessage::UserJoined(username) => {
            window.println(&format!("{} has joined", username));
            comm.write(RCMessage::RequestUserList);
        },
        RCMessage::UserLeft(username) => {
            window.println(&format!("{} has disconnected", username));
            comm.write(RCMessage::RequestUserList);
        },
        RCMessage::UserList(list) => {
            //window.println(format!("{:?}", list).to_string());
            user_win.clear();
            user_win.println("-- User List --");

            for i in list {
                user_win.println(&i);
            }
        },
        RCMessage::PrivateMessage { username, message } => {
            window.println(&format!("WHISPER FROM {}: {}", username, message));
        },
        m @ _ => window.println(&format!("Unknown message: {:?}", m))
    };
}
